<?php
require("animal.php");
require("frog.php");
require("ape.php");

$sheep = new Animal("shaun");
echo "nama hewan : $sheep->nama <br>";
echo "legs : $sheep->legs <br>";
echo "cold blooded : $sheep->cold_blooded <br><br>";

$kodok = new frog("buduk");
echo "nama hewan : $kodok->nama <br>";
echo "legs : $kodok->legs <br>";
echo "cold blooded : $kodok->cold_blooded <br>";
echo $kodok->jump() . "<br><br>";

$sungkong = new ape("kera sakti");
echo "nama hewan : $sungkong->nama <br>";
echo "legs : $sungkong->legs <br>";
echo "cold blooded : $sungkong->cold_blooded <br>";
echo $sungkong->yell();
"<br></br>";

?>